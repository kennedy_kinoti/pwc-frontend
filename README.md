# PwC Interview Assessment

## Getting Started
- Install Nodejs from [Nodejs Official Page](https://nodejs.org/en/)
- Open your terminal
- Navigate to the project
- Run `npm install` or `yarn install` if you use [Yarn](https://yarnpkg.com/en/)
- Run `npm run dev` or `yarn serve` to start a local development server
- A new tab will be opened in yougclour browser

You can also run additional npm tasks such as
- `npm run build` to build your app for production
- `npm run lint` to run linting.

## Authors
Kennedy Kinoti

## Acknowledgments
Hat tip to anyone whose code was used

> It’s not a bug – it’s an undocumented feature.
>> ~ Anonymous


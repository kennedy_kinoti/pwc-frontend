// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import Vue from 'vue'
import './components'
import './plugins'
import { sync } from 'vuex-router-sync'

// Application imports
import store from '@/store'
import App from './App'
import i18n from '@/i18n'
import router from '@/router'

// Import the functions you need from the SDKs you need
// Sync store with router
sync(store, router)
var firebase = require('firebase/app')

Vue.config.productionTip = false
Vue.use(firebase)

const config = {
  apiKey: 'AIzaSyDoI-SgiKbB8RZOD2QI3rwlGtpQHhWqjPs',
  authDomain: 'pwc-frontend.firebaseapp.com',
  databaseURL: 'https://pwc-frontend-default-rtdb.asia-southeast1.firebasedatabase.app',
  projectId: 'pwc-frontend',
  storageBucket: 'pwc-frontend.appspot.com',
  messagingSenderId: '909521488831',
  appId: '1:909521488831:web:97b50b9158a56f3cfbaf0a',
  measurementId: 'G-GS7CN6C3E5'
}

// Initialize Firebase
firebase.initializeApp(config)
Vue.prototype.$firebase = firebase

/* eslint-disable no-new */
new Vue({
  i18n,
  router,
  store,
  firebase,
  render: h => h(App)
}).$mount('#app')

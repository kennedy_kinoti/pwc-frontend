/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */
export default [
  {
    path: '/companies',
    name: 'Companies',
    view: 'Companies'
  },
  {
    path: '/restricted',
    name: 'Restricted',
    view: 'Restricted'
  },
  {
    path: '/search',
    name: 'Search',
    view: 'Search'
  }
]
